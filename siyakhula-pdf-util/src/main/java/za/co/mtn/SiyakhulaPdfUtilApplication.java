package za.co.mtn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import za.co.mtn.siyakhula.pdfutil.service.PdfService;

@SpringBootApplication
public class SiyakhulaPdfUtilApplication implements CommandLineRunner {

	@Autowired
	private PdfService pdfService;

	public static void main(String[] args) {
		SpringApplication.run(SiyakhulaPdfUtilApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		pdfService.pdfTypeCheck();
	}
}
