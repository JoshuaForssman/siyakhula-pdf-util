package za.co.mtn.siyakhula.pdfutil.service;

import com.itextpdf.forms.PdfAcroForm;
import com.itextpdf.forms.xfa.XfaForm;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


@Service
@Slf4j
public class PdfService {

  @Value("${application.InputDirectoryContainingXml}")
  private String inputDirectoryContainingXml;

  @Value("${application.InputPdfTemplateLocation}")
  private String inputPdfTemplateLocation;

  @Value("${application.OutputPdfLocation}")
  private String outputPdfLocation;

  private File[] readFilesFromDirectory(String directory) {

    File folder = new File(directory);
    File[] listOfFiles = folder.listFiles();

    return listOfFiles;
  }


  public void generatePdf(String pdfTypeName, String xmlFileName) throws IOException {

    try{
      log.info("Started processing " + xmlFileName);
      PdfReader pdfReader = new PdfReader(inputPdfTemplateLocation + pdfTypeName);
      pdfReader.setUnethicalReading(true);
      PdfDocument pdfDocument = new PdfDocument(pdfReader, new PdfWriter(outputPdfLocation + pdfTypeName));

      PdfAcroForm form = PdfAcroForm.getAcroForm(pdfDocument, true);
      XfaForm xfa = form.getXfaForm();
      xfa.fillXfaForm(new FileInputStream(inputDirectoryContainingXml + xmlFileName));
      xfa.write(pdfDocument);

      pdfDocument.close();

      log.info("Pdf generated");

    }catch (Exception e){
      log.error("Generating document failed, please ensure you have provided correct pdf xml");
    }
  }

  public void pdfTypeCheck() throws IOException {

    File[] listOfFiles = readFilesFromDirectory(inputDirectoryContainingXml);

    if(listOfFiles == null) {
      log.info("No xml files found");
      return;
    }

    log.info("Files retrieved, process started");

    for(File file :listOfFiles){

      if (file.getName().equals("Usage.xml")){
        generatePdf("Usage.pdf", file.getName());
      }else if(file.getName().equals("Statement.xml")){
        generatePdf("Statement.pdf", file.getName());
      }else if(file.getName().equals("Invoice.xml")){
        generatePdf("Invoice.pdf", file.getName());
      }else {
        log.info("Incorrect xml provided, check name of file.");
      }

    }
  }
}
